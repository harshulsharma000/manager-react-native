//@flow
import React,{Component} from 'react';
import {Scene, Router,Actions} from 'react-native-router-flux';
import LoginForm from './components/LoginForm';

class RouterComponent extends Component{
	render(){
		return(
			<Router sceneStyle = {{padding: 65}}>
				<Scene key = "auth">
					<Scene key = "login" component = {LoginForm} title = "Please Log In" />
				</Scene>
			</Router>
		);
	}
}
export default RouterComponent;