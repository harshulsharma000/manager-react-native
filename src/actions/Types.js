export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_ATTEMPT = 'login_attempt';
export const LOGIN_FAIL = 'login_failure';
export const WEAK_PASS = 'weak_password';