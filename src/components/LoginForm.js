//@flow
import React,{Component} from 'react';
import {Text,View} from 'react-native';
import {Card,CardSection,Input,Button,Spinner} from './common';
import {LoginAttempt} from '../actions';
import {connect} from 'react-redux';

class LoginForm extends Component{
	state = {email: "",password:"", error: "",loading: false};
	renderButton(){
		 if (this.props.loading) {
				return <Spinner size="large" />;
		}

		return (
			<Button onPress={this.onButtonPress.bind(this)}>
				Log In
			</Button>
		);
	}
	onButtonPress(){
		this.props.LoginAttempt(this.state.email,this.state.password);
	}
	componentWillMount(){
		const {email,password,error,loading} = this.props;
		this.setState({email,password,error,loading});
	}
	componentWillReceiveProps(nextProps){
		const {email,password,error,loading} = nextProps;
		this.setState({email,password,error,loading});
	}
	render(){
		return(
			<View>
				<CardSection>
					<Input
						value = {this.state.email}
						onChangeText = {email => this.setState({email})}
						label = 'Email:'
						placeholder = 'user@email.com'
					/>
				</CardSection>
				<CardSection>
					<Input
						placeholder = "password"
						secureTextEntry = {true}
						label = "Password:"
						value = {this.state.password}
						onChangeText = {password => this.setState({password})}
					/>
				</CardSection>
				<CardSection>
					{this.renderButton()}
				</CardSection>
				<View style = {{height: 40}}/>
				<View>
					<Text style = {styles.errorTextStyle}>
						{this.state.error}
					</Text>
				</View>
			</View>
		);
	}
}
const  mapStateToProps = (state, ownProps) => {
	const auth = state.auth;
	return {...auth};
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

export default connect(mapStateToProps,{LoginAttempt})(LoginForm);