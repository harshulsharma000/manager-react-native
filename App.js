//@flow
import React,{Component} from 'react';
import {Text,View} from 'react-native';
import {Provider} from 'react-redux';
import {createStore,applyMiddleware} from 'redux';
import firebase from 'firebase';
import reducers from './src/reducers';
import ReduxThunk from 'redux-thunk';
import LoginForm from './src/components/LoginForm';
//import Router from './src/Router';

class App extends Component{
	componentWillMount(){
		var config = {
			apiKey: "AIzaSyDlW1K-pRC3wnzw6XBdI_7hCzaQ1iQAGgw",
			authDomain: "manager-792a4.firebaseapp.com",
			databaseURL: "https://manager-792a4.firebaseio.com",
			projectId: "manager-792a4",
			storageBucket: "manager-792a4.appspot.com",
			messagingSenderId: "687366841744"
		};
		firebase.initializeApp(config);
	}
	render(){
		const store = createStore(reducers,{},applyMiddleware(ReduxThunk));
		return(
			<Provider store = {store}>
				<LoginForm />
			</Provider>
		);
	}
}

export default App;